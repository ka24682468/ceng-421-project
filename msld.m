function J = msld(image, w)
	
	% extract green colour channel for each image as blood vessesl are most prominent there
	gc = image(:,:,2);
	
	% invert intensity in green channel image
	gc = im2double(gc);
	igc = 1-gc;
	
	img = igc;

	% get line detection masks
	masks = msldMasks(w);

	% masks{scale}(rows, columns, orientation);

	% scale masks based on their length
	for i = 2:8
		K = nnz(masks{i}(:,:,1));
		masks{i}(:,:,:) = masks{i}(:,:,:)./K;
	end

	% get image dimensions
	[M N] = size(img);

	% get average intensities at each scale
	w = 15;
	Iavg = cell(8,1);
	avgKernels = cell(8,1);

	% create averaging kernels for each line scale
	i = 1;
	for k = 1:2:w
		avgKernels{i} = ones(k)./(k^2);
		i = i + 1;
	end

	% compute average pixel intensity at each scale
	for k = 1:8
		Iavg{k} = imfilter(img, avgKernels{k}, 'replicate');
	end

	% get line responses at each scale
	Imax = cell(8,1);
	Im = zeros(M,N,12);
	for k = 1:8
		for j = 1:12
			Im(:,:,j) = imfilter(img, masks{k}(:,:,j), 'replicate');
		end
		Imax{k} = max(Im, [], 3);
	end

	% calculate line detector reponse over multiple scales
	Rw = cell(8,1);
	for k = 1:8
		Rw{k} = Imax{k} - Iavg{k};
	end

	%standardize response values
	R = cell(8,1);
	for k = 2:8
		for j = 1:12
			mu = mean2(Rw{k});
			s = std2(Rw{k});
			R{k} = (Rw{k} - mu)./s;
		end
	end

	% combine multiple scale responses into single response image
	S = sum(cat(3, R{2:8}), 3) + img;
	S = S./(8+1);

	% return result
	J = S;

end
