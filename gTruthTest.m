clc;
clear 'all';

test_path = 'test-images/stare-images/';
result_path = 'test-results/';

test_images = dir(strcat(test_path, '*.ppm'));
test_images = {test_images.name}';

mkdir('test-results');

for k = 1:length(test_images)
	read_path = strcat(test_path, test_images{k});
	I = imread(read_path);
	J = msld(I, 15);
	write_path = strcat(result_path, test_images{k});
	imwrite(J, write_path);
end
